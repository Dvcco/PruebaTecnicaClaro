FROM openjdk:8
COPY build/libs/ms-pruebatecnicaclaro-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]