package com.pruebatecnica.claro.service;

import com.pruebatecnica.claro.dto.UsuarioDto;
import com.pruebatecnica.claro.exception.ExcepcionPruebaTecnica;
import com.pruebatecnica.claro.mocks.ObjetoMockUsuario;
import com.pruebatecnica.claro.model.UsuarioModel;
import com.pruebatecnica.claro.repository.UsuarioRepository;
import com.pruebatecnica.claro.service.impl.UsuarioServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@SpringBootTest
public class UsuarioServiceTest {

    @InjectMocks
    private UsuarioServiceImpl usuarioService;

    @Mock
    private UsuarioRepository usuarioRepository;

    @Before
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test(expected = ExcepcionPruebaTecnica.class)
    public void dadoGuardarUsuarioCuandoNumeroIdentificacionExisteEntoncesRespondeLaExcepcion() {
        Optional<UsuarioModel> optionalUsuarioModel = Optional.of(ObjetoMockUsuario.usuarioModelo());
        UsuarioDto dto = ObjetoMockUsuario.usuarioDto();
        when(usuarioRepository.findByNumeroIdentificacion(dto.getNumeroIdentificacion())).thenReturn(optionalUsuarioModel);
        Mono<UsuarioDto> guardarUsuario = usuarioService.guardarUsuario(dto);
    }

    @Test
    public void dadoGuardarUsuarioCuandoNumeroIdentificacionNoExisteEntoncesGuardarElRegistro() {
        UsuarioDto dto = ObjetoMockUsuario.usuarioDto();
        UsuarioModel usuarioModel = ObjetoMockUsuario.usuarioModelo();
        Optional<UsuarioModel> optionalUsuarioModel = ObjetoMockUsuario.optionalUsuarioModelNoPresent();
        when(usuarioRepository.findByNumeroIdentificacion(dto.getNumeroIdentificacion())).thenReturn(optionalUsuarioModel);
        when(usuarioRepository.save(Mockito.any(UsuarioModel.class))).thenReturn(usuarioModel);
        Mono<UsuarioDto> guardarUsuario = usuarioService.guardarUsuario(dto);
        UsuarioDto response = guardarUsuario.block();
        assertNotNull(response);
    }

    @Test(expected = ExcepcionPruebaTecnica.class)
    public void dadoEditarUsuarioCuandoNumeroIdentificacionNoexisteEntoncesRespondeExcepcion() {
        UsuarioDto dto = ObjetoMockUsuario.usuarioDto();
        Optional<UsuarioModel> optionalUsuarioModel = ObjetoMockUsuario.optionalUsuarioModelNoPresent();
        when(usuarioRepository.findByNumeroIdentificacion(dto.getNumeroIdentificacion())).thenReturn(optionalUsuarioModel);
        Mono<UsuarioDto> editarUsuario = usuarioService.editarUsuario(dto);
    }

    @Test(expected = ExcepcionPruebaTecnica.class)
    public void dadoEditarUsuarioCuandoCorreoElectronicoYaExisteEntoncesRespondeExcepcion() {
        UsuarioDto dto = ObjetoMockUsuario.usuarioDto();
        Optional<UsuarioModel> optionalUsuarioModel = Optional.of(ObjetoMockUsuario.usuarioModelo());
        when(usuarioRepository.findByNumeroIdentificacion(dto.getNumeroIdentificacion())).thenReturn(optionalUsuarioModel);
        when(usuarioRepository.findByCorreoElectronico(dto.getCorreoElectronico())).thenReturn(optionalUsuarioModel);
        Mono<UsuarioDto> editarUsuario = usuarioService.editarUsuario(dto);
    }

    @Test
    public void dadoEditarUsuarioCuandoElCorreoElectronicoNoExisteEntoncesEditaCorrectamente() {
        UsuarioDto dto = ObjetoMockUsuario.usuarioDto1();
        Optional<UsuarioModel> optionalUsuarioModel = Optional.of(ObjetoMockUsuario.usuarioModelo());
        UsuarioModel usuarioModel = ObjetoMockUsuario.usuarioModelo();
        when(usuarioRepository.findByNumeroIdentificacion(dto.getNumeroIdentificacion())).thenReturn(optionalUsuarioModel);
        when(usuarioRepository.findByNumeroIdentificacion(dto.getNumeroIdentificacion())).thenReturn(optionalUsuarioModel);
        when(usuarioRepository.findByCorreoElectronico(dto.getCorreoElectronico())).thenReturn(optionalUsuarioModel);
        when(usuarioRepository.save(Mockito.any(UsuarioModel.class))).thenReturn(usuarioModel);
        Mono<UsuarioDto> editarUsuario = usuarioService.editarUsuario(dto);
        UsuarioDto response = editarUsuario.block();
        assertNotNull(response);
    }

    @Test(expected = ExcepcionPruebaTecnica.class)
    public void dadoBuscarUsuarioPorDocumentoCuandoElNumeroIdentificacionNoExisteEntoncesRespondeExcepcion() {
        UsuarioDto dto = ObjetoMockUsuario.usuarioDto();
        Optional<UsuarioModel> optionalUsuarioModel = ObjetoMockUsuario.optionalUsuarioModelNoPresent();
        when(usuarioRepository.findByNumeroIdentificacion(dto.getNumeroIdentificacion())).thenReturn(optionalUsuarioModel);
        Mono<UsuarioDto> buscarUsuarioPorDocumento = usuarioService.buscarUsuarioPorDocumento(dto);
    }

    @Test
    public void dadoBuscarUsuarioPorDocumentoCuandoElNumeroIdentificacionExisteEntoncesRetornaDtoConLaInformacion() {
        UsuarioDto dto = ObjetoMockUsuario.usuarioDto();
        Optional<UsuarioModel> optionalUsuarioModelData = ObjetoMockUsuario.optionUsuarioModelData();
        when(usuarioRepository.findByNumeroIdentificacion(dto.getNumeroIdentificacion())).thenReturn(optionalUsuarioModelData);
        Mono<UsuarioDto> buscarUsuarioPorDocumento = usuarioService.buscarUsuarioPorDocumento(dto);
        assertNotNull(buscarUsuarioPorDocumento.block());

    }

    @Test(expected = ExcepcionPruebaTecnica.class)
    public void dadoBuscarUsuarioPorNombreApellidoCuandoElNombreYApellidoNoExisteEntoncesRespondeExcepcion() {
        UsuarioDto dto = ObjetoMockUsuario.usuarioDto();
        Optional<UsuarioModel> optionalUsuarioModel = ObjetoMockUsuario.optionalUsuarioModelNoPresent();
        when(usuarioRepository.findByNombreAndApellido(dto.getNombre(), dto.getApellido())).thenReturn(optionalUsuarioModel);
        Mono<UsuarioDto> buscarUsuarioPorNombreApellido = usuarioService.buscarUsuarioPorNombreApellido(dto);
    }

    @Test
    public void dadoBuscarUsuarioPorNombreApellidoCuandoElNombreYApellidoExisteEntoncesRetornaDtoConLaInformacion() {
        UsuarioDto dto = ObjetoMockUsuario.usuarioDto();
        Optional<UsuarioModel> optionalUsuarioModelData = ObjetoMockUsuario.optionUsuarioModelData();
        when(usuarioRepository.findByNombreAndApellido(dto.getNombre(), dto.getApellido())).thenReturn(optionalUsuarioModelData);
        Mono<UsuarioDto> buscarUsuarioPorNombreApellido = usuarioService.buscarUsuarioPorNombreApellido(dto);
        assertNotNull(buscarUsuarioPorNombreApellido.block());
    }

    @Test(expected = ExcepcionPruebaTecnica.class)
    public void dadoBuscarUsuarioPorFechaCreacionYNombreCuandoLaFechaCreacionYNombreNoExisteEntoncesRespondeExcepcion() {
        UsuarioDto dto = ObjetoMockUsuario.usuarioDto();
        Optional<UsuarioModel> optionalUsuarioModel = ObjetoMockUsuario.optionalUsuarioModelNoPresent();
        when(usuarioRepository.findByFechaCreacionAndNombre(dto.getFechaCreacion(), dto.getNombre())).thenReturn(optionalUsuarioModel);
        Mono<UsuarioDto> buscarPorFechaNombre = usuarioService.buscarPorFechaNombre(dto);
    }

    @Test
    public void dadoBuscarUsuarioPorFechaCreacionYNombreCuandoLaFechaCreacionYNombreExisteEntoncesRetornaDtoConLaInformacion() {
        UsuarioDto dto = ObjetoMockUsuario.usuarioDto();
        Optional<UsuarioModel> optionalUsuarioModelData = ObjetoMockUsuario.optionUsuarioModelData();
        when(usuarioRepository.findByFechaCreacionAndNombre(Mockito.any(), Mockito.any())).thenReturn(optionalUsuarioModelData);
        Mono<UsuarioDto> buscarPorFechaNombre = usuarioService.buscarPorFechaNombre(dto);
        assertNotNull(buscarPorFechaNombre.block());
    }

    @Test
    public void dadoListarTodosLosUsuariosCuandoElProcesoEsExitosoRetornaLaListaDeObjetos() {
        List<UsuarioModel> listaModelo = ObjetoMockUsuario.listaModelo();
        when(usuarioRepository.findAll()).thenReturn(listaModelo);
        Mono<List<UsuarioDto>> listarUsuarios = usuarioService.listarUsuarios();
        List<UsuarioDto> lista = listarUsuarios.block();
        assertNotNull(lista);
    }

    @Test(expected = ExcepcionPruebaTecnica.class)
    public void dadoBorrarRegistroProcesoExitoso() {
        UsuarioDto dto = ObjetoMockUsuario.usuarioDto();
        usuarioService.borrarUsuario(dto);
    }
}