package com.pruebatecnica.claro.mocks;

import com.pruebatecnica.claro.dto.UsuarioDto;
import com.pruebatecnica.claro.model.UsuarioModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class ObjetoMockUsuario {

    public static UsuarioModel usuarioModelo(){
        return UsuarioModel.builder()
                .idUsuario(1L)
                .numeroIdentificacion("1022426561")
                .tipoDocumento("CC")
                .nombre("Daniel")
                .apellido("Casallas")
                .edad(26)
                .genero('M')
                .numeroCelular("3207666705")
                .correoElectronico("dacco1214@gmail.com")
                .fechaCreacion("2023-01-22")
                .build();
    }

    public static UsuarioDto usuarioDto(){
        return UsuarioDto.builder()
                .idUsuario(1L)
                .numeroIdentificacion("1022426561")
                .tipoDocumento("CC")
                .nombre("Daniel")
                .apellido("Casallas")
                .edad(26)
                .genero('M')
                .numeroCelular("3207666705")
                .correoElectronico("dacco1214@gmail.com")
                .fechaCreacion(formatearFecha(new Date()))
                .fechaActualizacion(new Date())
                .build();
    }
    public static UsuarioDto usuarioDto1(){
        return UsuarioDto.builder()
                .idUsuario(1L)
                .numeroIdentificacion("1022426561")
                .tipoDocumento("CC")
                .nombre("Daniel")
                .apellido("Casallas")
                .edad(26)
                .genero('M')
                .numeroCelular("3207666705")
                .correoElectronico("dacco1213@gmail.com")
                .fechaActualizacion(new Date())
                .build();
    }

    public static List<UsuarioModel> listaModelo(){
        List<UsuarioModel> lista = new ArrayList<>();
        lista.add(usuarioModelo());
        return lista;
    }
    public static Optional<UsuarioModel> optionalUsuarioModelNoPresent(){
        return Optional.ofNullable(null);
    }

    public static Optional<UsuarioModel> optionUsuarioModelData(){
        return Optional.of(usuarioModelo());
    }
    private static String formatearFecha(Date fecha){
        return new SimpleDateFormat("yyyy-MM-dd").format(fecha);
    }
}

