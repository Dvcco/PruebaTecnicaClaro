package com.pruebatecnica.claro.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity(name ="usuario")
@ToString
@AllArgsConstructor
@Builder
@Getter
@Setter
@NoArgsConstructor
@Table(name = "usuario", schema = "db_pruebatecnicaclaro")
public class UsuarioModel  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_USUARIO")
    private Long idUsuario;

    @Column(name = "NUM_DOCUMENTO")
    private String numeroIdentificacion;

    @Column(name = "TIPO_DOCUMENTO")
    private String tipoDocumento;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "APELLIDO")
    private String apellido;

    @Column(name = "EDAD")
    private Integer edad;

    @Column(name = "GENERO")
    private char genero;

    @Column(name = "NUM_CELULAR")
    private String numeroCelular;

    @Column(name = "CORREO")
    private String correoElectronico;

    @Column(name = "FECHA_CREACION")
    private String fechaCreacion;

    @Column(name = "FECHA_ACTUALIZACION")
    private Date fechaActualizacion;
}
