package com.pruebatecnica.claro.service;

import com.pruebatecnica.claro.dto.UsuarioDto;
import reactor.core.publisher.Mono;

import java.util.List;

public interface UsuarioService {

    Mono<UsuarioDto> guardarUsuario(UsuarioDto dto);
    Mono<UsuarioDto> editarUsuario(UsuarioDto dto);
    Mono<UsuarioDto> buscarUsuarioPorDocumento (UsuarioDto dto);
    Mono<UsuarioDto> buscarUsuarioPorNombreApellido(UsuarioDto dto);
    Mono<UsuarioDto> buscarPorFechaNombre(UsuarioDto dto);
    Mono<List<UsuarioDto>> listarUsuarios();
    void borrarUsuario(UsuarioDto dto)  ;
}
