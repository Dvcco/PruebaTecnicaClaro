package com.pruebatecnica.claro.service.impl;

import com.pruebatecnica.claro.dto.UsuarioDto;
import com.pruebatecnica.claro.exception.ExcepcionPruebaTecnica;
import com.pruebatecnica.claro.model.UsuarioModel;
import com.pruebatecnica.claro.repository.UsuarioRepository;
import com.pruebatecnica.claro.service.UsuarioService;
import com.pruebatecnica.claro.util.Constantes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public Mono<UsuarioDto> guardarUsuario(UsuarioDto dto) {

        if (usuarioRepository.findByNumeroIdentificacion(dto.getNumeroIdentificacion()).isPresent()) {
            throw new ExcepcionPruebaTecnica(Constantes.COD001, HttpStatus.BAD_REQUEST, Constantes.DOCUMENTO_YA_EXISTE);
        }

        UsuarioModel usuarioModel = new UsuarioModel();
        usuarioModel.setNumeroIdentificacion(dto.getNumeroIdentificacion());
        usuarioModel.setTipoDocumento(dto.getTipoDocumento());
        usuarioModel.setNombre(dto.getNombre());
        usuarioModel.setApellido(dto.getApellido());
        usuarioModel.setEdad(dto.getEdad());
        usuarioModel.setGenero(dto.getGenero());
        usuarioModel.setNumeroCelular(dto.getNumeroCelular());
        usuarioModel.setCorreoElectronico(dto.getCorreoElectronico());
        usuarioModel.setFechaCreacion(formatearFecha(new Date()));
        usuarioRepository.save(usuarioModel);

        return Mono.just(dto);
    }

    @Override
    public Mono<UsuarioDto> editarUsuario(UsuarioDto dto) {

        if (!usuarioRepository.findByNumeroIdentificacion(dto.getNumeroIdentificacion()).isPresent()) {
            throw new ExcepcionPruebaTecnica(Constantes.COD001, HttpStatus.BAD_REQUEST, Constantes.DOCUMENTO_NO_EXISTE);
        }

        Optional<UsuarioModel> validacion = usuarioRepository.findByNumeroIdentificacion(dto.getNumeroIdentificacion());

        if (validacion.get().getCorreoElectronico().equals(dto.getCorreoElectronico()) && usuarioRepository.findByCorreoElectronico(dto.getCorreoElectronico()).isPresent()) {
            throw new ExcepcionPruebaTecnica(Constantes.COD001, HttpStatus.BAD_REQUEST, Constantes.CORREO_EN_USO);
        }

        validacion.get().setNumeroIdentificacion(dto.getNumeroIdentificacion());
        validacion.get().setTipoDocumento(dto.getTipoDocumento());
        validacion.get().setNombre(dto.getNombre());
        validacion.get().setApellido(dto.getApellido());
        validacion.get().setEdad(dto.getEdad());
        validacion.get().setGenero(dto.getGenero());
        validacion.get().setNumeroCelular(dto.getNumeroCelular());
        validacion.get().setCorreoElectronico(dto.getCorreoElectronico());
        validacion.get().setFechaActualizacion(new Date());
        usuarioRepository.save(validacion.get());
        return Mono.just(dto);
    }

    @Override
    public Mono<UsuarioDto> buscarUsuarioPorDocumento(UsuarioDto dto) {
        Optional<UsuarioModel> numeroDocumento = usuarioRepository.findByNumeroIdentificacion(dto.getNumeroIdentificacion());
        if (!numeroDocumento.isPresent()) {
            throw new ExcepcionPruebaTecnica(Constantes.COD001, HttpStatus.BAD_REQUEST, Constantes.DOCUMENTO_NO_EXISTE);
        } else {
            UsuarioModel model = numeroDocumento.get();
            UsuarioDto usuarioDto = UsuarioDto.builder().
                    idUsuario(model.getIdUsuario())
                    .numeroIdentificacion(model.getNumeroIdentificacion())
                    .tipoDocumento(model.getTipoDocumento())
                    .nombre(model.getNombre())
                    .apellido(model.getApellido())
                    .edad(model.getEdad())
                    .genero(model.getGenero())
                    .numeroCelular(model.getNumeroCelular())
                    .correoElectronico(model.getCorreoElectronico())
                    .fechaCreacion(model.getFechaCreacion()).build();
            return Mono.just(usuarioDto);
        }
    }

    @Override
    public Mono<UsuarioDto> buscarUsuarioPorNombreApellido(UsuarioDto dto) {
        Optional<UsuarioModel> nombreAndApellido = usuarioRepository.findByNombreAndApellido(dto.getNombre(), dto.getApellido());
        if (!nombreAndApellido.isPresent()) {
            throw new ExcepcionPruebaTecnica(Constantes.COD001, HttpStatus.BAD_REQUEST, Constantes.NOMBRE_APELLIDO_NO_EXISTE);
        } else {
            UsuarioModel model = nombreAndApellido.get();
            UsuarioDto usuarioDto = UsuarioDto.builder().
                    idUsuario(model.getIdUsuario())
                    .numeroIdentificacion(model.getNumeroIdentificacion())
                    .tipoDocumento(model.getTipoDocumento())
                    .nombre(model.getNombre())
                    .apellido(model.getApellido())
                    .edad(model.getEdad())
                    .genero(model.getGenero())
                    .numeroCelular(model.getNumeroCelular())
                    .correoElectronico(model.getCorreoElectronico())
                    .fechaCreacion(model.getFechaCreacion()).build();
            return Mono.just(usuarioDto);
        }
    }

    @Override
    public Mono<UsuarioDto> buscarPorFechaNombre(UsuarioDto dto) {
        Optional<UsuarioModel> fechaAndNombre = usuarioRepository.findByFechaCreacionAndNombre(formatearFecha(new Date()), dto.getNombre());
        if (!fechaAndNombre.isPresent()) {
            throw new ExcepcionPruebaTecnica(Constantes.COD001, HttpStatus.BAD_REQUEST, Constantes.FECHA_APELLIDO_NO_EXISTE);
        } else {
            UsuarioModel model = fechaAndNombre.get();
            UsuarioDto usuarioDto = UsuarioDto.builder().
                    idUsuario(model.getIdUsuario())
                    .numeroIdentificacion(model.getNumeroIdentificacion())
                    .tipoDocumento(model.getTipoDocumento())
                    .nombre(model.getNombre())
                    .apellido(model.getApellido())
                    .edad(model.getEdad())
                    .genero(model.getGenero())
                    .numeroCelular(model.getNumeroCelular())
                    .correoElectronico(model.getCorreoElectronico())
                    .fechaCreacion(model.getFechaCreacion()).build();
            return Mono.just(usuarioDto);
        }
    }

    @Override
    public Mono<List<UsuarioDto>> listarUsuarios() {
        List<UsuarioModel> listaModelo = usuarioRepository.findAll();
        return Mono.just(listaModelo.stream().map(dto -> UsuarioDto.builder()
                .idUsuario(dto.getIdUsuario())
                .numeroIdentificacion(dto.getNumeroIdentificacion())
                .tipoDocumento(dto.getTipoDocumento())
                .nombre(dto.getNombre())
                .apellido(dto.getApellido())
                .edad(dto.getEdad())
                .genero(dto.getGenero())
                .numeroCelular(dto.getNumeroCelular())
                .correoElectronico(dto.getCorreoElectronico())
                .fechaCreacion(dto.getFechaCreacion())
                .build()).collect(Collectors.toList()));
    }

    @Override
    public void borrarUsuario(UsuarioDto dto) {
        usuarioRepository.deleteByNumeroIdentificacion(dto.getNumeroIdentificacion());
        throw new ExcepcionPruebaTecnica("",HttpStatus.OK, Constantes.REGISTRO_BORRADO);
    }

    private static String formatearFecha(Date fecha){
        try{
            return new SimpleDateFormat("yyyy-MM-dd").format(fecha);
        }catch (Exception e){
            throw new ExcepcionPruebaTecnica(Constantes.COD001, HttpStatus.BAD_REQUEST, Constantes.FECHA_NO_FORMATEADA);
        }
    }
}

