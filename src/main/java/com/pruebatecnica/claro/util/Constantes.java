package com.pruebatecnica.claro.util;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Constantes {
    public static final String COD001 =  "COD001";
    public static final String DOCUMENTO_YA_EXISTE = "El número de identificación ingresado ya existe";
    public static final String DOCUMENTO_NO_EXISTE = "El número de identificación ingresado no existe";
    public static final String CORREO_EN_USO = "El correo electronico ya se encuentra registrado y/o se encuentra en uso";
    public static final String NOMBRE_APELLIDO_NO_EXISTE = "El nombre y apellido digitado no existe";
    public static final String FECHA_APELLIDO_NO_EXISTE = "La fecha y nombre digitado no existe";
    public static final String FECHA_NO_FORMATEADA = "La fecha no se pudo formatear";
    public static final String REGISTRO_BORRADO = "Se borro el registro correctamente";
}
