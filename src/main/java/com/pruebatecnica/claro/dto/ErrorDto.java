package com.pruebatecnica.claro.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
@ApiModel(value = "ErrorDto", description = "Esta clase representa los codigos de error del sistema")
public class ErrorDto {

    @ApiModelProperty(value = "code", required = true, dataType = "String", example = "0", position = 1)
    private String codigoError;
    @ApiModelProperty(value = "message", required = true, dataType = "String", example = "0", position = 2)
    private String descripcion;
}
