package com.pruebatecnica.claro.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UsuarioDto {

    private Long idUsuario;

    private String numeroIdentificacion;

    private String tipoDocumento;

    private String nombre;

    private String apellido;

    private Integer edad;

    private char genero;

    private String numeroCelular;

    private String correoElectronico;

    private String fechaCreacion;

    private Date fechaActualizacion;
}
