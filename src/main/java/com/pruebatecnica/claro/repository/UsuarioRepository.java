package com.pruebatecnica.claro.repository;

import com.pruebatecnica.claro.model.UsuarioModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
@Repository
public interface UsuarioRepository extends JpaRepository<UsuarioModel, Long> {

    Optional<UsuarioModel> findByNumeroIdentificacion(String numeroIdentificacion);

    Optional<UsuarioModel> findByCorreoElectronico(String correoElectronico);

    Optional<UsuarioModel> findByNombreAndApellido(String nombre, String apellido);

    Optional<UsuarioModel> findByFechaCreacionAndNombre(String fecha, String nombre);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM usuario u WHERE u.numeroIdentificacion = :numeroIdentificacion")
    void deleteByNumeroIdentificacion(@Param("numeroIdentificacion") String numeroIdentificacion);
}
