package com.pruebatecnica.claro.controller.advice;

import com.pruebatecnica.claro.dto.ErrorDto;
import com.pruebatecnica.claro.exception.ExcepcionPruebaTecnica;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class AdviceController {

    @ExceptionHandler(value = ExcepcionPruebaTecnica.class)
    public ResponseEntity<ErrorDto> runTimeExeptionHandler(ExcepcionPruebaTecnica ex) {
        return ResponseEntity.status(ex.getCodeHttp()).body(ErrorDto.builder().codigoError(ex.getCodigoError()).descripcion(ex.getDescripcion()).build());
    }
}
