package com.pruebatecnica.claro.controller;

import com.pruebatecnica.claro.dto.UsuarioDto;
import com.pruebatecnica.claro.service.UsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;
@RestController
@Api(tags = "Usuario Controller")
@RequestMapping("/pruebatecnica/claro")
@CrossOrigin(origins = "*")
public class UsuarioController {
    @Autowired
    private UsuarioService usuarioService;

    @ApiOperation(value = "Guardar Usuario", response = UsuarioDto.class, httpMethod = "POST")
    @ApiResponses({ @ApiResponse(code = 200,message = " Operacion exitosa, guarda un usuario")})
    @PostMapping(value ="/usuario/guardar")
    public Mono<UsuarioDto> guardarUsuario(@RequestBody UsuarioDto dto){
        return usuarioService.guardarUsuario(dto);
    }

    @ApiOperation(value = "Editar Usuario", response = UsuarioDto.class, httpMethod = "POST")
    @ApiResponses({ @ApiResponse(code = 200,message = " Operacion exitosa, edita un usuario")})
    @PostMapping(value = "/usuario/editar")
    public Mono<UsuarioDto> editarUsuario(@RequestBody UsuarioDto dto){
        return usuarioService.editarUsuario(dto);
    }

    @ApiOperation(value = "Buscar Usuario por Numero Documento", response = UsuarioDto.class, httpMethod = "POST")
    @ApiResponses({ @ApiResponse(code = 200,message = " Operacion exitosa, busca un numero de documento")})
    @PostMapping(value = "/usuario/buscarusuariopordocumento")
    public Mono<UsuarioDto> buscarUsuarioPorDocumento(@RequestBody UsuarioDto dto){
        return usuarioService.buscarUsuarioPorDocumento(dto);
    }

    @ApiOperation(value = "Buscar Usuario por Nombre y Apellido", response = UsuarioDto.class, httpMethod = "POST")
    @ApiResponses({ @ApiResponse(code = 200,message = " Operacion exitosa, busca un usuario por nombre y apellido")})
    @PostMapping(value = "/usuario/buscarusuariopornombreapellido")
    public Mono<UsuarioDto> buscarUsuarioPorNombreApellido(@RequestBody UsuarioDto dto){
        return usuarioService.buscarUsuarioPorNombreApellido(dto);
    }

    @ApiOperation(value = "Buscar Usuario por Fecha creacion y Nombre", response = UsuarioDto.class, httpMethod = "POST")
    @ApiResponses({ @ApiResponse(code = 200,message = " Operacion exitosa, busca un usuario por fecha creacion y nombre")})
    @PostMapping(value = "/usuario/buscarusuarioporfechanombre")
    public Mono<UsuarioDto> buscarPorFechaNombre(@RequestBody UsuarioDto dto){
        return usuarioService.buscarPorFechaNombre(dto);
    }

    @ApiOperation(value = "Listar todos los usuarios", response = UsuarioDto.class, httpMethod = "POST")
    @ApiResponses({ @ApiResponse(code = 200,message = " Operacion exitosa, lista todos los usuarios")})
    @PostMapping(value = "/usuario/listarusuarios")
    public Mono<List<UsuarioDto>> listarUsuarios(){
        return usuarioService.listarUsuarios();
    }

    @ApiOperation(value = "Elimina un usuario por su Numero de documento", response = UsuarioDto.class, httpMethod = "DELETE")
    @ApiResponses({ @ApiResponse(code = 200,message = " Operacion exitosa, elimina un usuario por su numero de documento")})
    @DeleteMapping(value = "/usuario/borrarusuario")
    public void borrarUsuario(@RequestBody  UsuarioDto dto)  {
        usuarioService.borrarUsuario(dto);
    }
}
